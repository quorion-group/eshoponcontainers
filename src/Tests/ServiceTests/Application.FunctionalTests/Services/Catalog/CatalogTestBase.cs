﻿using BuildingBlocks.IntegrationEventLogEF;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Services.Catalog.API;
using Services.Catalog.API.Extensions;
using Services.Catalog.API.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Application.FunctionalTests.Services.Catalog
{
    public class CatalogTestBase
    {
        public TestServer CreateTestServer()
        {
            var webhostBuilder = new WebHostBuilder()
                .UseContentRoot(Path.GetDirectoryName(Assembly.GetAssembly(typeof(CatalogTestBase)).Location))
                .ConfigureAppConfiguration(cb =>
                {
                    cb.AddJsonFile("Services/Catalog/appsettings.json")
                    .AddEnvironmentVariables();
                })
                .UseStartup<Startup>();

            var testServer = new TestServer(webhostBuilder);

            testServer.Host.MigrateDbContext<CatalogContext>((contex, serviceProvider) =>
            {
                var environment = serviceProvider.GetService<IWebHostEnvironment>();
                var logger = serviceProvider.GetService<ILogger<CatalogContextSeed>>();
                if (environment != null && logger != null)
                    new CatalogContextSeed().SeedAsync(contex, environment, logger).Wait();
            }).MigrateDbContext<IntegrationEventLogContext>((_, _) => { });

            return testServer;
        }
    }
}
