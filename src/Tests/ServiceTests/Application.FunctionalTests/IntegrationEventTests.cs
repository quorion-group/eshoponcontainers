using Application.FunctionalTests.Services.Basket;
using Application.FunctionalTests.Services.Catalog;
using Newtonsoft.Json;
using Services.Basket.API.Model;
using Services.Catalog.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Application.FunctionalTests
{
    public class IntegrationEventTests
    {
        private const string USER_ID = "orion";

        [Fact]
        public async Task UpdatePriceInCatalog_WillUpdatePriceInBasket()
        {
            using var catalogServer = new CatalogTestBase().CreateTestServer();
            using var basketServer = new BasketTestBase().CreateTestServer();

            using var catalogClient = catalogServer.CreateClient();
            using var basketClient = basketServer.CreateClient();
            IEnumerable<CatalogItem> products = await GetCatalogsAsync(catalogClient);

            CustomerBasket basket = GenerateCustomerBasket(products.Take(2));
            await CreateBasketAsync(basketClient, basket);

            var catalog = products.FirstOrDefault();
            catalog.Price += 1;

            await UpdateCatalogAsync(catalogClient, catalog);

            int count = 0;
            bool updatedUnitPrice = false;
            while (count < 20 && !updatedUnitPrice)
            {
                CustomerBasket basketRes = await GetCustomerBasketAsync(basketClient);
                updatedUnitPrice = catalog.Price == basketRes.Items.First(item => item.ProductId == catalog.Id).UnitPrice;

                await Task.Delay(100);
            }

            Assert.True(updatedUnitPrice);
        }

        private static async Task CreateBasketAsync(HttpClient basketClient, CustomerBasket basket)
        {
            var basketContent = new StringContent(JsonConvert.SerializeObject(basket), Encoding.UTF8, "application/json");
            await basketClient.PostAsync("api/v1/basket", basketContent);
        }

        private static async Task<IEnumerable<CatalogItem>> GetCatalogsAsync(HttpClient catalogClient)
        {
            var productsResponse = await catalogClient.GetAsync("api/v1/catalog/items");
            var productsStr = await productsResponse.Content.ReadAsStringAsync();
            var products = JsonConvert.DeserializeObject<PaginateItemsViewModel<CatalogItem>>(productsStr);
            return products.Items;
        }

        private static CustomerBasket GenerateCustomerBasket(IEnumerable<CatalogItem> subProducts)
        {
            return new CustomerBasket
            {
                BuyerId = USER_ID,
                Items = new List<BasketItem>(subProducts.Select(p => new BasketItem
                {
                    Id = Guid.NewGuid().ToString(),
                    UnitPrice = p.Price,
                    OldUnitPrice = p.Price,
                    PictureUrl = p.PictureUri,
                    ProductId = p.Id,
                    ProductName = p.Name,
                    Quantity = 1
                }))
            };
        }

        private static async Task UpdateCatalogAsync(HttpClient catalogClient, CatalogItem catalog)
        {
            HttpContent content = new StringContent(JsonConvert.SerializeObject(catalog), encoding: Encoding.UTF8, "application/json");
            var response = await catalogClient.PutAsync("api/v1/catalog/items", content);
            var res = await response.Content.ReadAsStringAsync();
        }

        private static async Task<CustomerBasket> GetCustomerBasketAsync(HttpClient basketClient)
        {
            var basketResponse = await basketClient.GetAsync("api/v1/basket/" + USER_ID);
            var basketStr = await basketResponse.Content.ReadAsStringAsync();
            var basketRes = JsonConvert.DeserializeObject<CustomerBasket>(basketStr);
            return basketRes;
        }
    }
}