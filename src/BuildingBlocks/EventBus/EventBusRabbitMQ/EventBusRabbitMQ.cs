﻿using Autofac;
using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.Events;
using BuildingBlocks.EventBusRabbitMQ;
using EventBus;
using EventBus.Extensions;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;

namespace BuildingBlocks.EventBus.EventBusRabbitMQ;

public class EventBusRabbitMQ : IEventBus
{
    private const string EXCHANGE = "eshop_event_bus";
    private const string AUTOFAC_SCOPE_NAME = "eshop_event_bus";

    private readonly IRabbitMQPersistentConnection _persistentConnection;
    private readonly ILogger<EventBusRabbitMQ> _logger;
    private readonly IEventBusSubscriptionsManager _subManager;
    private readonly ILifetimeScope _autofac;

    private IModel _consumerChannel;
    private readonly string _queueName;

    public EventBusRabbitMQ(IRabbitMQPersistentConnection persistentConnection, ILogger<EventBusRabbitMQ> logger, IEventBusSubscriptionsManager subManager, string queueName, ILifetimeScope autofac)
    {
        _persistentConnection = persistentConnection ?? throw new ArgumentNullException(nameof(persistentConnection));
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _subManager = subManager ?? throw new ArgumentNullException(nameof(subManager));
        _queueName = queueName;
        _autofac = autofac ?? throw new ArgumentNullException(nameof(autofac));
        _consumerChannel = CreateConsumerChannel();
    }

    public void Publish(IntegrationEvent @event)
    {
        if (!_persistentConnection.IsConnected)
            _persistentConnection.TryConnect();

        var body = JsonSerializer.SerializeToUtf8Bytes(@event, @event.GetType(), new JsonSerializerOptions { WriteIndented = true });

        var policy = Policy.Handle<SocketException>()
               .Or<BrokerUnreachableException>()
               .WaitAndRetry(5, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
               {
                   _logger.LogWarning(ex, "Could not publish event: {EventId} after {Timeout}s ({ExceptionMessage})", @event.Id, $"{time.TotalSeconds:n1}", ex.Message);
               });

        string eventName = @event.GetType().Name;

        _logger.LogTrace("Creating RabbitMQ channel to publish event: {EventId} ({EventName})", @event.Id, eventName);

        using var channel = _persistentConnection.CreateModel();

        _logger.LogTrace("Declaring RabbitMQ exchange to publish event: {EventId}", @event.Id);

        channel.ExchangeDeclare(exchange: EXCHANGE, type: "direct");

        policy.Execute(() =>
        {
            var properties = channel.CreateBasicProperties();
            properties.DeliveryMode = 2;

            _logger.LogTrace("Publishing event to RabbitMQ exchange: {EventId}", @event.Id);

            channel.BasicPublish(
                exchange: EXCHANGE,
                routingKey: eventName,
                mandatory: true,
                basicProperties: properties,
                body: body);
        });
    }

    public void Subscribe<Evt, EvtHandler>()
        where Evt : IntegrationEvent
        where EvtHandler : IIntegrationEventHandler<Evt>
    {
        var eventName = _subManager.GetEventKey<Evt>();

        DoInternalSubcription(eventName);

        _logger.LogInformation("Subcribing to event {EventName} with {EventHandler}", eventName, typeof(EvtHandler).GetGenericTypeName());

        _subManager.AddSubcription<Evt, EvtHandler>();
        StartBasicConsume();
    }

    private void DoInternalSubcription(string eventName)
    {
        var containsKey = _subManager.HasSubcriptionsForEvent(eventName);
        if (containsKey) return;

        if (!_persistentConnection.IsConnected)
            _persistentConnection.TryConnect();

        _consumerChannel.QueueBind(queue: _queueName,
            exchange: EXCHANGE,
            routingKey: eventName);
    }

    private IModel CreateConsumerChannel()
    {
        if (!_persistentConnection.IsConnected)
        {
            _persistentConnection.TryConnect();
        }

        _logger.LogInformation("Creating RabbitMQ consumer channel");

        var channel = _persistentConnection.CreateModel();

        channel.ExchangeDeclare(
            exchange: EXCHANGE,
            type: "direct");

        channel.QueueDeclare(
            queue: _queueName,
            durable: true,
            exclusive: false,
            autoDelete: false,
            arguments: null);

        channel.CallbackException += (sender, earg) =>
        {
            _logger.LogWarning(earg.Exception, "Recreating RabitMQ consumer channel");

            _consumerChannel.Dispose();
            _consumerChannel = CreateConsumerChannel();
            StartBasicConsume();
        };

        return channel;
    }

    private void StartBasicConsume()
    {
        _logger.LogTrace("Starting RabbitMQ basic consume");

        if (_consumerChannel is null)
        {
            _logger.LogError("StartBasicConsume can't call on _consumerChannel == null");
            return;
        }

        var consumer = new AsyncEventingBasicConsumer(_consumerChannel);

        consumer.Received += Consumer_Received;

        _consumerChannel.BasicConsume(
            queue: _queueName,
            autoAck: false,
            consumer: consumer);
    }

    private async Task Consumer_Received(object sender, BasicDeliverEventArgs @event)
    {
        var eventName = @event.RoutingKey;
        var message = Encoding.UTF8.GetString(@event.Body.Span);

        try
        {
            await ProcessEvent(eventName, message);
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "---- ERROR Processing message \"{Message}\"", message);
        }

        _consumerChannel.BasicAck(@event.DeliveryTag, multiple: false);
    }

    private async Task ProcessEvent(string eventName, string message)
    {
        _logger.LogInformation("Processing RabbitMQ event: {EventName}", eventName);

        if (!_subManager.HasSubcriptionsForEvent(eventName))
        {
            _logger.LogWarning("No subcription for RabbitMQ event: {EventName}", eventName);
            return;
        }

        using var scope = _autofac.BeginLifetimeScope(AUTOFAC_SCOPE_NAME);
        var subcriptions = _subManager.GetHandlersForEvent(eventName);
        foreach (var subcription in subcriptions)
        {
            if (subcription.IsDynamic)
            {
                var handler = scope.ResolveOptional(subcription.HandlerType) as IDynamicIntegrationEventHandler;
                if (handler is null)
                {
                    _logger.LogWarning("Could not resolve handler {HandlerType} for event {EventName}", subcription.HandlerType, eventName);
                    continue;
                }
                using dynamic eventData = JsonDocument.Parse(message);
                await Task.Yield();
                await handler.Handle(eventData);
            }
            else
            {
                var handler = scope.ResolveOptional(subcription.HandlerType);
                if (handler is null)
                {
                    _logger.LogWarning("Could not resolve handler {HandlerType} for event {EventName}", subcription.HandlerType, eventName);
                    continue;
                }
                var eventType = _subManager.GetEventTypeByName(eventName);
                var integrationEvent = JsonSerializer.Deserialize(message, eventType, new JsonSerializerOptions { PropertyNameCaseInsensitive = true }); ;
                var concreteType = typeof(IIntegrationEventHandler<>).MakeGenericType(eventType);

                await Task.Yield();
                await (Task)concreteType.GetMethod("Handle").Invoke(handler, new object[] { integrationEvent });
            }
        }
    }
}
