﻿using BuildingBlocks.EventBus.Events;
using Microsoft.EntityFrameworkCore.Storage;

namespace BuildingBlocks.IntegrationEventLogEF.Services
{
    public interface IIntegrationEventLogService
    {
        Task SaveEventAsync(IntegrationEvent @event, IDbContextTransaction transaction);
        Task MarkEventAsInProgressAsync(Guid eventId);
        Task MarkEventAsPublishedAsync(Guid eventId);
        Task MarkEventAsPublishedFailedAsync(Guid eventId);
    }
}
