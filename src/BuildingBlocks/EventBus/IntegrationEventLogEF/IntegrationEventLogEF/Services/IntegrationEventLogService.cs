﻿using BuildingBlocks.EventBus.Events;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace BuildingBlocks.IntegrationEventLogEF.Services
{
    public class IntegrationEventLogService : IIntegrationEventLogService, IDisposable
    {
        private readonly IntegrationEventLogContext _eventLogContext;
        private bool _disposedValue;

        public IntegrationEventLogService(DbContextOptions<IntegrationEventLogContext> dbContextOptions)
        {
            _eventLogContext = new IntegrationEventLogContext(dbContextOptions);
        }

        public Task SaveEventAsync(IntegrationEvent @event, IDbContextTransaction transaction)
        {
            if (transaction is null) throw new ArgumentNullException(nameof(transaction));

            var entry = new IntegrationEventLogEntry(@event, transaction.TransactionId);

            _eventLogContext.Database.UseTransaction(transaction.GetDbTransaction()); //we want to use the same
            //transaction with main transaction coz the main transaction called BeginTransaction and Commit
            //so it will help us ensure that main transaction can roll back if any exception throw inside this method (SaveEventAsync)
            //therefore the main context and event log context have to use the same db connection, same database
            _eventLogContext.Add(entry);

            return _eventLogContext.SaveChangesAsync();
        }

        public async Task MarkEventAsInProgressAsync(Guid eventId)
        {
            await UpdateEventLogState(eventId, EventState.InProgress);
        }

        public async Task MarkEventAsPublishedAsync(Guid eventId)
        {
            await UpdateEventLogState(eventId, EventState.Published);
        }

        public async Task MarkEventAsPublishedFailedAsync(Guid eventId)
        {
            await UpdateEventLogState(eventId, EventState.PublishedFailed);
        }

        private async Task UpdateEventLogState(Guid eventId, EventState state)
        {
            var eventLogEntry = await _eventLogContext.IntegrationEventLogs.SingleOrDefaultAsync(e => e.EventId == eventId);

            if (eventLogEntry is null) throw new KeyNotFoundException();

            eventLogEntry.State = state;

            if (state == EventState.InProgress)
                eventLogEntry.TimesSent++;

            _eventLogContext.Update(eventLogEntry);
            await _eventLogContext.SaveChangesAsync();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposedValue)
            {
                if (disposing)
                {
                    _eventLogContext?.Dispose();
                }

                _disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
