﻿using BuildingBlocks.EventBus.Events;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace BuildingBlocks.IntegrationEventLogEF;
public class IntegrationEventLogEntry
{
    private IntegrationEventLogEntry() { }

    public IntegrationEventLogEntry(IntegrationEvent @event, Guid transactionId)
    {
        EventId = @event.Id;
        EventTypeName = @event.GetType().Name;
        State = EventState.NotPublished;
        CreationTime = DateTime.UtcNow;
        Content = JsonSerializer.Serialize(@event, @event.GetType(), new JsonSerializerOptions { WriteIndented = true });
        TransactionId = transactionId;
    }

    public Guid EventId { get; private set; }
    public string EventTypeName { get; private set; }
    [NotMapped]
    public string? EventTypeShortName => EventTypeName.Split(".")?.Last();
    [NotMapped]
    public IntegrationEvent IntegrationEvent { get; private set; }
    public EventState State { get; set; }
    public int TimesSent { get; set; }
    public DateTime CreationTime { get; private set; }
    public string Content { get; private set; }
    public Guid TransactionId { get; private set; }
}
