﻿using Microsoft.EntityFrameworkCore;

namespace BuildingBlocks.IntegrationEventLogEF.Utilities;

public class ResilientTransaction
{
    private DbContext _dbContext;

    public ResilientTransaction(DbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public static ResilientTransaction New(DbContext dbContext) => new(dbContext);

    public async Task ExecuteAsync(Func<Task> action)
    {
        var strategy = _dbContext.Database.CreateExecutionStrategy();
        await strategy.ExecuteAsync(async () =>
        {
            using var transaction = _dbContext.Database.BeginTransaction();

            await action();

            transaction.Commit();
        });
    }
}
