﻿namespace EventBus
{
    public class SubcriptionInfo
    {
        public bool IsDynamic { get; }
        public Type HandlerType { get; }

        public SubcriptionInfo(bool isDynamic, Type handlerType)
        {
            IsDynamic = isDynamic;
            HandlerType = handlerType;
        }

        public static SubcriptionInfo Dynamic(Type handlerType) => new SubcriptionInfo(true, handlerType);

        public static SubcriptionInfo Typed(Type handlerType) => new SubcriptionInfo(false, handlerType);
    }
}
