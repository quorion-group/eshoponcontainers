﻿using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.Events;

namespace EventBus
{
    public class InMemoryEventBusSubscriptionsManager : IEventBusSubscriptionsManager
    {
        private readonly Dictionary<string, List<SubcriptionInfo>> _handlers;
        private readonly List<Type> _eventTypes;

        public event EventHandler<string> OnEventRemoved;

        public InMemoryEventBusSubscriptionsManager()
        {
            _handlers = new Dictionary<string, List<SubcriptionInfo>>();
            _eventTypes = new List<Type>();
        }

        public bool IsEmpty => _handlers is { Count: 0 };

        public void AddSubcription<Evt, EvtHandler>()
            where Evt : IntegrationEvent
            where EvtHandler : IIntegrationEventHandler<Evt>
        {
            var eventName = GetEventKey<Evt>();

            DoAddSubcription(eventName, typeof(EvtHandler), false);

            if (!_eventTypes.Contains(typeof(Evt)))
                _eventTypes.Add(typeof(Evt));
        }

        private void DoAddSubcription(string eventName, Type handlerType, bool isDynamic)
        {
            if (!HasSubcriptionsForEvent(eventName))
                _handlers.Add(eventName, new List<SubcriptionInfo>());

            if (_handlers[eventName].Any(sub => sub.HandlerType == handlerType))
                throw new ArgumentException(
                    $"Handler Type {handlerType.Name} already registered for '{eventName}'");

            if (isDynamic)
                _handlers[eventName].Add(SubcriptionInfo.Dynamic(handlerType));
            else
                _handlers[eventName].Add(SubcriptionInfo.Typed(handlerType));
        }

        public string GetEventKey<Evt>() where Evt : IntegrationEvent
        {
            return typeof(Evt).Name;
        }

        public Type GetEventTypeByName(string eventName)
        {
            return _eventTypes.First(t => t.Name == eventName);   
        }

        public IEnumerable<SubcriptionInfo> GetHandlersForEvent(string eventName)
        {
            return _handlers[eventName];
        }

        public bool HasSubcriptionsForEvent(string eventName)
        {
            return _handlers.ContainsKey(eventName);
        }
    }
}
