﻿using BuildingBlocks.EventBus.Events;

namespace BuildingBlocks.EventBus.Abstractions;

public interface IEventBus
{
    void Publish(IntegrationEvent @event);

    void Subscribe<Evt, EvtHandler>() where Evt : IntegrationEvent where EvtHandler : IIntegrationEventHandler<Evt>;
}
