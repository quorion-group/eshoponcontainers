﻿using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventBus
{
    public interface IEventBusSubscriptionsManager
    {
        bool IsEmpty { get; }
        event EventHandler<string> OnEventRemoved;

        void AddSubcription<Evt, EvtHandler>() where Evt : IntegrationEvent where EvtHandler : IIntegrationEventHandler<Evt>;

        bool HasSubcriptionsForEvent(string eventName);

        string GetEventKey<Evt>() where Evt : IntegrationEvent;

        IEnumerable<SubcriptionInfo> GetHandlersForEvent(string eventName);

        Type GetEventTypeByName(string eventName);
    }
}
