using Microsoft.AspNetCore;
using Services.Ordering.API;
using Serilog;

 var configuration = GetConfiguration();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

try
{
    var webhost = BuildWebhost(args, configuration);
    webhost.Run();

    Log.Information("Application is started");
}
catch (Exception ex)
{
    Log.Error("Fail to start application. {Message} - {StackTrace}", ex.Message, ex.StackTrace);
}
finally
{
    Log.CloseAndFlush();
}

IWebHost BuildWebhost(string[] args, IConfiguration configuration) =>
    WebHost.CreateDefaultBuilder(args)
        .UseStartup<Startup>()
        .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
        .CaptureStartupErrors(false)
        .UseContentRoot(Directory.GetCurrentDirectory())
        .Build();


IConfiguration GetConfiguration()
{
    return new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
        .AddEnvironmentVariables()
        .Build();
}

public partial class Program
{
    public static string Namespace = typeof(OrderingSetting).Namespace;
    public static string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

    public static IConfiguration GetConfiguration()
    {
        return new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
            .Build();
    }
}