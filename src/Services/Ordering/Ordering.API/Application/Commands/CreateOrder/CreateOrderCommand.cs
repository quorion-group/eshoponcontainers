﻿using MediatR;
using Services.Ordering.API.Extensions;
using Services.Ordering.API.Model;
using System.Runtime.Serialization;

namespace Services.Ordering.API.Application.Commands;

[DataContract]
public class CreateOrderCommand : IRequest<bool>
{
    private readonly List<OrderItemDto> _orderItems;

    [DataMember]
    public string UserId { get; private set; }

    [DataMember]
    public string UserName { get; private set; }

    [DataMember]
    public string City { get; private set; }

    [DataMember]
    public string Street { get; private set; }

    [DataMember]
    public string State { get; private set; }

    [DataMember]
    public string Country { get; private set; }

    [DataMember]
    public string CardNumber { get; private set; }

    [DataMember]
    public string CardHolderName { get; private set; }

    [DataMember]
    public DateTime CardExpiration { get; private set; }

    [DataMember]
    public string CardSecurityNumber { get; private set; }

    [DataMember]
    public int CardTypeId { get; private set; }

    [DataMember]
    public IEnumerable<OrderItemDto> OrderItems => _orderItems;

    public CreateOrderCommand()
    {
        _orderItems = new List<OrderItemDto>();
    }

    public CreateOrderCommand(List<BasketItem> basketItems, string userId, string userName, string city, string street, string state,
        string country, string cardNumber, string cardHolderName, string cardSecurityNumber, DateTime cardExpiration, int cardTypeId) : this()
    {
        UserId = userId;
        UserName = userName;
        City = city;
        Street = street;
        State = state;
        Country = country;
        CardNumber = cardNumber;
        CardHolderName = cardHolderName;
        CardSecurityNumber = cardSecurityNumber;
        CardExpiration = cardExpiration;
        CardTypeId = cardTypeId;

        _orderItems = basketItems.ToOrderItemsDto().ToList();
    }
}
