﻿using MediatR;
using Services.Ordering.API.Extensions;
using Services.Ordering.Domain.AggregatesModel.OrderAggregate;

namespace Services.Ordering.API.Application.Commands;

public class CreateOrderDraftCommandHandler : IRequestHandler<CreateOrderDraftCommand, OrderDraftDto>
{
    public Task<OrderDraftDto> Handle(CreateOrderDraftCommand message, CancellationToken cancellationToken)
    {
        var order = Order.NewDraft();
        var orderItems = message.BasketItems.Select(i => i.ToOrderItemDto());
        foreach(var item in orderItems)
        {
            order.AddOrderItem(item.ProductId, item.ProductName, item.UnitPrice, 0, item.PictureUrl, item.Units);
        }

        return Task.FromResult(OrderDraftDto.FromOrder(order));
    }
}
