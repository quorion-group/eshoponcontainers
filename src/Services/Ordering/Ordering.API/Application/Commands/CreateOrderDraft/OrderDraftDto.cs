﻿using Services.Ordering.Domain.AggregatesModel.OrderAggregate;

namespace Services.Ordering.API.Application.Commands;

public record OrderDraftDto
{
    public IEnumerable<OrderItemDto> OrderItems { get; init; }

    public decimal Total { get; init; }

    public static OrderDraftDto FromOrder(Order order)
    {
        return new OrderDraftDto
        {
            Total = order.GetTotal(),
            OrderItems = order.OrderItems.Select(oi => new OrderItemDto
            {
                Discount = oi.GetCurrentDiscount(),
                Units = oi.GetUnits(),
                PictureUrl = oi.GetPictureUrl(),
                ProductName = oi.GetProductName(),
                UnitPrice = oi.GetUnitPrice(),
                ProductId = oi.ProductId,
            })
        };
    }
}
