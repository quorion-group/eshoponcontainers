﻿using Autofac;
using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.EventBusRabbitMQ;
using BuildingBlocks.EventBusRabbitMQ;
using BuildingBlocks.IntegrationEventLogEF;
using BuildingBlocks.IntegrationEventLogEF.Services;
using EventBus;
using Microsoft.EntityFrameworkCore;
using Ordering.API.Application.IntegrationEvents;
using Ordering.Infrastructure;
using RabbitMQ.Client;
using System.Reflection;

namespace Services.Ordering.API;
public class Startup
{
    public IConfiguration Configuration { get; private set; }

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services
            .AddCustomMVC()
            .AddSwaggerGen()
            .AddCustomDbContext(Configuration)
            .AddIntegrationService(Configuration)
            .AddEventBus(Configuration)
            .AddCustomSetting(Configuration);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ordering.API V1");
            });
        }

        app.UseHttpsRedirection();

        app.UseRouting();
    }
}

public static class CustomExtensions
{
    public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<OrderingContext>(options =>
        {
            options.UseSqlServer(configuration["ConnectionString"], sqlOptions =>
            {
                sqlOptions.MigrationsAssembly(typeof(Program).GetTypeInfo().Assembly.GetName().Name);
                sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(60), errorNumbersToAdd: null);
            });
        });

        services.AddDbContext<IntegrationEventLogContext>(options =>
        {
            options.UseSqlServer(configuration["ConnectionString"], 
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly(typeof(Program).GetTypeInfo().Assembly.GetName().Name);
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(60), errorNumbersToAdd: null);
                });
        });

        return services;
    }

    public static IServiceCollection AddIntegrationService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<Func<DbContextOptions<IntegrationEventLogContext>, IIntegrationEventLogService>>(sp =>
        {
            return (DbContextOptions<IntegrationEventLogContext> options) => new IntegrationEventLogService(options);
        });

        services.AddTransient<IOrderingIntegrationEventService, OrderingIntegrationEventService>();
        services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
        {
            var factory = new ConnectionFactory
            {
                HostName = configuration["EventBusHost"],
                DispatchConsumersAsync = true
            };

            if (!String.IsNullOrWhiteSpace(configuration["EventBusUserName"]))
                factory.UserName = configuration["EventBusUserName"];

            if (!String.IsNullOrWhiteSpace(configuration["EventBusPassword"]))
                factory.Password = configuration["EventBusPassword"];

            var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

            int retryCount = 5;
            if (!string.IsNullOrWhiteSpace(configuration["EventBusRetryCount"])
                && int.TryParse(configuration["EventBusRetryCount"], out int retryCountResult))
            {
                retryCount = retryCountResult;
            }

            return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
        });

        return services;
    }

    public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
        {
            var subcriptionClientName = configuration["SubscriptionClientName"];
            var persistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            var subManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
            var iLifeTimeScope = sp.GetRequiredService<ILifetimeScope>();

            return new EventBusRabbitMQ(persistentConnection, logger, subManager, subcriptionClientName, iLifeTimeScope);
        });
        services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

        return services;
    }

    public static IServiceCollection AddCustomMVC(this IServiceCollection services)
    {
        services.AddControllers();
        services.AddCors(options =>
            options.AddPolicy("CorsPolicy", opt =>
                opt.SetIsOriginAllowed(_ => true)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
        ));

        return services;
    }

    public static IServiceCollection AddCustomSetting(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<OrderingSetting>(configuration);

        return services;
    }
}
