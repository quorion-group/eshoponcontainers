﻿using Services.Ordering.API.Application.Commands;
using Services.Ordering.API.Model;

namespace Services.Ordering.API.Extensions;

public static class BasketItemExtensions
{
    public static IEnumerable<OrderItemDto> ToOrderItemsDto(this IEnumerable<BasketItem> basketItems)
    {
        foreach(var item in basketItems)
        {
            yield return item.ToOrderItemDto();
        }
    }

    public static OrderItemDto ToOrderItemDto (this BasketItem basketItem)
    {
        return new OrderItemDto
        {
            UnitPrice = basketItem.UnitPrice,
            PictureUrl = basketItem.PictureUrl,
            ProductId = basketItem.ProductId,
            ProductName = basketItem.ProductName,
            Units = basketItem.Quantity
        };
    }
}
