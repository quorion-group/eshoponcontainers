﻿namespace Services.Ordering.API.Model;

public class CustomerBasket
{
    public string BuyerId { get; set; }

    public List<BasketItem> Items { get; set; } = new List<BasketItem>();

    public CustomerBasket(string customerId, List<BasketItem> items)
    {
        BuyerId = customerId;
        Items = items;
    }
}
