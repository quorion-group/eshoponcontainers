﻿using Microsoft.AspNetCore.Mvc;

namespace Services.Ordering.API.Controllers;

public class HomeController : Controller
{
    [Route("/")]
    [HttpGet]
    public IActionResult Index()
    {
        return Redirect("~/swagger");
    }
}
