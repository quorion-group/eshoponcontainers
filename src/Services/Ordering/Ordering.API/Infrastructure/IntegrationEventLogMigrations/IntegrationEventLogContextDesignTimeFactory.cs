﻿using BuildingBlocks.IntegrationEventLogEF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Ordering.API.Infrastructure.IntegrationEventLogMigrations;

public class IntegrationEventLogContextDesignTimeFactory : IDesignTimeDbContextFactory<IntegrationEventLogContext>
{
    public IntegrationEventLogContext CreateDbContext(string[] args)
    {
        var optionBuilder = new DbContextOptionsBuilder<IntegrationEventLogContext>();
        optionBuilder.UseSqlServer(".", sqlOptions =>
        {
            sqlOptions.MigrationsAssembly(GetType().Assembly.GetName().Name);
        });

        return new IntegrationEventLogContext(optionBuilder.Options);
    }
}
