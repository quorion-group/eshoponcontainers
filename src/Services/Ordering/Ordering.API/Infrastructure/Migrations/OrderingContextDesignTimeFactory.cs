﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Ordering.Infrastructure;

namespace Ordering.API.Infrastructure.Migrations
{
    public class OrderingContextDesignTimeFactory : IDesignTimeDbContextFactory<OrderingContext>
    {
        public OrderingContext CreateDbContext(string[] args)
        {
            var optionBuilder = new DbContextOptionsBuilder<OrderingContext>();
            optionBuilder.UseSqlServer(".", sqlOptions =>
            {
                sqlOptions.MigrationsAssembly(GetType().Assembly.GetName().Name);
            });

            return new OrderingContext(optionBuilder.Options);
        }
    }
}
