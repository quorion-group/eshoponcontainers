﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Ordering.Domain.Exceptions;
public class OrderingDomainException : Exception
{
    public OrderingDomainException(string message) : base(message) { }

    public OrderingDomainException(string message, Exception innerException) : base(message, innerException) { }
}
