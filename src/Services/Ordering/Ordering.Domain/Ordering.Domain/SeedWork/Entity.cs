﻿using MediatR;

namespace Services.Ordering.Domain.SeedWork;
public abstract class Entity
{
    int? _requestedHashCode;
    int _id;

    public virtual int Id
    {
        get { return _id; }
        protected set { _id = value; }
    }

    private List<INotification> _domainEvents;
    public IEnumerable<INotification> DomainEvents => _domainEvents.AsReadOnly();

    public void AddDomainEvent(INotification eventItem){
        _domainEvents = _domainEvents ?? new List<INotification>();
        _domainEvents.Add(eventItem);
    }

    public void RemoveDomainEvent(INotification eventItem)
    {
        _domainEvents?.Remove(eventItem);
    }

    public void ClearDomainEvents()
    {
        _domainEvents?.Clear();
    }

    public bool IsTransient()
    {
        return this.Id == default(Int32);
    }
}
