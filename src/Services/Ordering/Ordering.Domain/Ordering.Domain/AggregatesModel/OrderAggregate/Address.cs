﻿using Services.Ordering.Domain.SeedWork;

namespace Services.Ordering.Domain.AggregatesModel.OrderAggregate;

public class Address : ValueObject
{
    public string Street { get; private set; }
    public string City { get; private set; }
    public string State { get; set; }
    public string Country { get; set; }
    public string ZipCode { get; set; }

    public Address()
    {

    }

    public Address(string street, string city, string state, string country, string zipCode)
    {
        Street = street;
        City = city;
        State = state;
        Country = country;
        ZipCode = zipCode;
    }
}
