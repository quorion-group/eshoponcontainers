﻿using Services.Ordering.Domain.Exceptions;
using Services.Ordering.Domain.SeedWork;

namespace Services.Ordering.Domain.AggregatesModel.OrderAggregate;

public class OrderItem : Entity
{
    private string _productName;
    private string _pictureUrl;
    private decimal _unitPrice;
    private decimal _discount;
    private int _units;

    public int ProductId { get; private set; }

    protected OrderItem() { }

    public OrderItem(string productName, string pictureUrl, decimal unitPrice, decimal discount, int units, int productId)
    {
        if (units <= 0) throw new OrderingDomainException("Invalid number of units");

        if ((units * unitPrice) < discount)
            throw new OrderingDomainException("The total amount is lower than applied discount");

        ProductId = productId;

        _productName = productName;
        _pictureUrl = pictureUrl;
        _unitPrice = unitPrice;
        _discount = discount;
        _units = units;
    }

    public decimal GetCurrentDiscount() => _discount;

    public int GetUnits() => _units;

    public string GetProductName() => _productName;

    public string GetPictureUrl() => _pictureUrl;

    public decimal GetUnitPrice() => _unitPrice;

    public void SetNewDiscount(decimal discount)
    {
        if (discount < 0) throw new OrderingDomainException("Invalid discount");

        _discount = discount;
    }

    public void AddUnits(int units)
    {
        if (units < 0) throw new OrderingDomainException("Invalid units");

        _units += units;
    }
}
