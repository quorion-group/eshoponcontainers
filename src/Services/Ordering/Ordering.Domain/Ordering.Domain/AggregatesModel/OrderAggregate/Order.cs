﻿using Services.Ordering.Domain.Events;
using Services.Ordering.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Ordering.Domain.AggregatesModel.OrderAggregate;
public class Order : Entity
{
    private DateTime _orderDate;

    public Address Address { get; private set; }

    private int? _buyerId;
    public int? BuyerId => _buyerId;

    public OrderStatus OrderStatus { get; private set; }
    private int _orderStatusId;

    private string _description;

    private bool _isDraft;

    private List<OrderItem> _orderItems;
    public IReadOnlyCollection<OrderItem> OrderItems => _orderItems;

    private int? _paymentMethodId;

    public static Order NewDraft()
    {
        var order = new Order();
        order._isDraft = true;
        return order;
    }

    public Order()
    {
        _orderItems = new List<OrderItem>();
        _isDraft = false;
    }

    public Order(string userId, string userName, Address address, int cardTypeId, string cardNumber, string cardSecurityNumber,
        string cardHolderName, DateTime cardExpiration, int? buyerId = null, int? paymentMethodId = null) : this()
    {
        _buyerId = buyerId;
        _paymentMethodId = paymentMethodId;
        _orderStatusId = 1;
        _orderDate = DateTime.UtcNow;
        Address = address;

        AddOrderStaredDomainEvent(userId, userName, cardTypeId, cardNumber, cardSecurityNumber, cardHolderName, cardExpiration);
    }

    public void AddOrderItem(int productId, string productName, decimal unitPrice, decimal discount, string pictureUrl, int units = 1)
    {
        var existingOrderItemForProduct = _orderItems.SingleOrDefault(item => item.ProductId == productId);

        if(existingOrderItemForProduct != null)
        {
            if(discount > existingOrderItemForProduct.GetCurrentDiscount())
            {
                existingOrderItemForProduct.SetNewDiscount(discount);
            }

            existingOrderItemForProduct.AddUnits(units);
        } else
        {
            var orderItem = new OrderItem(productName, pictureUrl, unitPrice, discount, units, productId);
            _orderItems.Add(orderItem);
        }
    }

    public decimal GetTotal() => _orderItems.Sum(o => o.GetUnits());

    private void AddOrderStaredDomainEvent(string userId, string userName, int cardTypeId, string cardNumber,
        string cardSecurityNumber, string cardHolderName, DateTime cardExpiration)
    {
        var eventItem = new OrderStartedDomainEvent(this, userId, userName, cardTypeId, cardNumber,
                                                    cardSecurityNumber, cardHolderName, cardExpiration);

        AddDomainEvent(eventItem);
    }
}
