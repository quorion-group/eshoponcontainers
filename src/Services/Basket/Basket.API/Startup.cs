﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using Basket.API.Grpc;
using Basket.API.Infrastructure.Repositories;
using Basket.API.IntegrationEvents.EventHandling;
using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.EventBusRabbitMQ;
using BuildingBlocks.EventBusRabbitMQ;
using EventBus;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.OpenApi.Models;
using RabbitMQ.Client;
using Services.Basket.API.IntegrationEvents.Events;
using Services.Basket.API.Model;
using Services.Basket.API.Services;
using StackExchange.Redis;
using System.IdentityModel.Tokens.Jwt;

namespace Services.Basket.API;
public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; private set; }

    public IServiceProvider ConfigureServices(IServiceCollection services)
    {
        services.AddGrpc();

        services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "eShopOnContainers - Basket HTTP API",
                Version = "v1",
                Description = "The basket service http api"
            });

            options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.OAuth2,
                Flows = new OpenApiOAuthFlows
                {
                    Implicit = new OpenApiOAuthFlow
                    {
                        AuthorizationUrl = new Uri($"{Configuration.GetValue<string>("IdentityUrlExternal")}/connect/authorize"),
                        TokenUrl = new Uri($"{Configuration.GetValue<string>("IdentityUrlExternal")}/connnect/token"),
                        Scopes = new Dictionary<string, string>
                        {
                            { "basket", "Basket API" }
                        }
                    }
                }
            });
        });

        ConfigureAuthService(services);

        services.AddCors(options =>
            options.AddPolicy("CorsPolicy", opt =>
                opt.SetIsOriginAllowed(_ => true)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
        ));

        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        services.AddScoped<IIdentityService, IdentityService>();

        services.AddSingleton(sp =>
        {
            var configurationOption = ConfigurationOptions.Parse(Configuration.GetValue<string>("ConnectionString"), true);
            return ConnectionMultiplexer.Connect(configurationOption);
        });
        services.AddScoped<IBasketRepository, RedisBasketRepository>();

        services.AddIntegrationEventBus(Configuration);

        var containerBuilder = new ContainerBuilder();
        containerBuilder.Populate(services);

        return new AutofacServiceProvider(containerBuilder.Build());
    }

    private void ConfigureAuthService(IServiceCollection services)
    {
        JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }).AddJwtBearer(options =>
        {
            options.Authority = Configuration.GetValue<string>("IdentityUrl");
            options.RequireHttpsMetadata = false;
            options.Audience = "basket";
        });
    }

    public void ConfigureContainer(ContainerBuilder builder)
    {
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseRouting();
        app.UseCors("CorsPolicy");
        
        ConfigureAuth(app);

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapDefaultControllerRoute();
            endpoints.MapControllers();
            endpoints.MapGet("/_proto/", async ctx =>
            {
                ctx.Response.ContentType = "text/plain";
                using var fs = new FileStream(Path.Combine(env.ContentRootPath, "Proto", "basket.proto"), FileMode.Open, FileAccess.Read);
                using var sr = new StreamReader(fs);
                while(!sr.EndOfStream)
                {
                    var line = await sr.ReadLineAsync();
                    if (line != "/* >>" || line != "<< */")
                    {
                        await ctx.Response.WriteAsync(line);
                    }
                }
            });
            endpoints.MapGrpcService<BasketService>();
        });

        RegisterIntegrationEventHandler(app);
    }

    protected virtual void ConfigureAuth(IApplicationBuilder app)
    {
        app.UseAuthentication();
        app.UseAuthorization();
    }

    private static void RegisterIntegrationEventHandler(IApplicationBuilder app)
    {
        var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();
        eventBus.Subscribe<ProductPriceChangedIntegrationEvent, ProductPriceChangedIntegrationEventHandler>();
    }
}

public static class RegisterIntegrationEventBusSevice
{
    public static IServiceCollection AddIntegrationEventBus(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
        {
            var factory = new ConnectionFactory
            {
                HostName = configuration["EventBusHost"],
                DispatchConsumersAsync = true
            };

            if (!string.IsNullOrWhiteSpace(configuration["EventBusUserName"]))
                factory.UserName = configuration["EventBusUserName"];

            if (!string.IsNullOrWhiteSpace(configuration["EventBusPassword"]))
                factory.Password = configuration["EventBusPassword"];

            var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

            int retryCount = 5;
            if (!string.IsNullOrWhiteSpace(configuration["EventBusRetryCount"])
                && int.TryParse(configuration["EventBusRetryCount"], out int retryCountResult))
            {
                retryCount = retryCountResult;
            }

            return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
        });

        services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
        {
            var subscriptionClientName = configuration["SubscriptionClientName"];
            var persistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            var subManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
            var iLifeTimeScope = sp.GetRequiredService<ILifetimeScope>();

            return new EventBusRabbitMQ(persistentConnection, logger, subManager, subscriptionClientName, iLifeTimeScope);
        });
        services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
        services.AddTransient<ProductPriceChangedIntegrationEventHandler>();

        return services;
    }
};