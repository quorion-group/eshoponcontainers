﻿using BasketApi;
using Grpc.Core;
using static BasketApi.Basket;

namespace Basket.API.Grpc
{
    public class BasketService : BasketBase
    {
        private readonly ILogger<BasketService> _logger;

        public BasketService(ILogger<BasketService> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public override Task<CustomerBasketResponse> GetBasketById(BasketRequest request, ServerCallContext context)
        {
            return base.GetBasketById(request, context);
        }

        public override Task<CustomerBasketResponse> UpdateBasket(CustomerBasketRequest request, ServerCallContext context)
        {
            return base.UpdateBasket(request, context);
        }
    }
}
