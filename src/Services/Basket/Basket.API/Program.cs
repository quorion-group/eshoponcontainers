using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;
using Services.Basket.API;
using System.Net;

var configuration = GetConfiguration();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

try
{
    var host = BuildHost(configuration, args);

    host.Run();

    Log.Information("Application is started");
}
catch (Exception ex)
{
    Log.Error("Fail to start application! {Message} {StackTrace}", ex.Message, ex.StackTrace);
}
finally
{
    Log.CloseAndFlush();
}

IWebHost BuildHost(IConfiguration configuration, string[] args) =>
    WebHost.CreateDefaultBuilder(args)
        .UseSerilog()
        .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
        .CaptureStartupErrors(false)
        .UseStartup<Startup>()
        .ConfigureKestrel(options =>
        {
            var port = GetDefinedPorts(configuration);
            options.Listen(IPAddress.Any, port.httpPort, listenOptions =>
            {
                listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
            });

            options.Listen(IPAddress.Any, port.grpcPort, listenOptions =>
            {
                listenOptions.Protocols = HttpProtocols.Http2;
            });
        })
        .UseContentRoot(Directory.GetCurrentDirectory())
        .Build();

IConfiguration GetConfiguration()
{
    return new ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", optional: true)
        .AddEnvironmentVariables()
        .Build();
}

(int httpPort, int grpcPort) GetDefinedPorts(IConfiguration configuration)
{
    var grpcPort = configuration.GetValue("GRPC_PORT", 81);
    var httpPort = configuration.GetValue("HTTP_PORT", 80);

    return (httpPort, grpcPort);
}

public partial class Program
{
    public static string Namespace = typeof(Startup).Namespace;
    public static string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);
}
