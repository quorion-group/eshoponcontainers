﻿using BuildingBlocks.EventBus.Abstractions;
using Serilog.Context;
using Services.Basket.API.IntegrationEvents.Events;
using Services.Basket.API.Model;

namespace Basket.API.IntegrationEvents.EventHandling
{
    public class ProductPriceChangedIntegrationEventHandler : IIntegrationEventHandler<ProductPriceChangedIntegrationEvent>
    {
        private readonly ILogger<ProductPriceChangedIntegrationEventHandler> _logger;
        private readonly IBasketRepository _basketRepository;

        public ProductPriceChangedIntegrationEventHandler(ILogger<ProductPriceChangedIntegrationEventHandler> logger, IBasketRepository basketRepository)
        {
            _basketRepository = basketRepository ?? throw new ArgumentNullException(nameof(basketRepository));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Handle(ProductPriceChangedIntegrationEvent @event)
        {
            using (LogContext.PushProperty("IntegrationEventContext", $"{@event.Id}-{Program.AppName}"))
            {
                _logger.LogInformation("---- Handling integration event: {IntegrationEventId} at {AppName}", @event.Id, Program.AppName);

                var userIds = _basketRepository.GetUsers();

                foreach (var userId in userIds)
                {
                    var basket = await _basketRepository.GetBasketAsync(userId);

                    await UpdatePriceInBasketItems(basket, @event.ProductId, @event.NewPrice, @event.OldPrice);
                }
            }
        }

        private async Task UpdatePriceInBasketItems(CustomerBasket? basket, int productId, decimal newPrice, decimal oldPrice)
        {
            var itemsToUpdate = basket?.Items?.Where(item => item.ProductId == productId);

            if (itemsToUpdate is not null)
            {
                itemsToUpdate.ToList().ForEach(item =>
                {
                    item.UnitPrice = newPrice;
                    item.OldUnitPrice = oldPrice;
                });

                await _basketRepository.UpdateBasketAsync(basket);
            }
        }
    }
}
