﻿using BuildingBlocks.EventBus.Events;

namespace Services.Basket.API.IntegrationEvents.Events
{
    public record ProductPriceChangedIntegrationEvent : IntegrationEvent
    {
        public int ProductId { get; private init; }
        public decimal OldPrice { get; private init; }
        public decimal NewPrice { get; private init; }

        public ProductPriceChangedIntegrationEvent(int productId, decimal oldPrice, decimal newPrice)
        {
            ProductId = productId;
            OldPrice = oldPrice;
            NewPrice = newPrice;
        }
    }
}
