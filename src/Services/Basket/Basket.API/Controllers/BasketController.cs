﻿using BuildingBlocks.EventBus.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Basket.API.IntegrationEvents.Events;
using Services.Basket.API.Model;
using Services.Basket.API.Services;
using System.Net;
using System.Security.Claims;

namespace Services.Basket.API.Controllers;

[Route("api/v1/[controller]")]
[Authorize]
[ApiController]
public class BasketController : ControllerBase
{
    private readonly ILogger<BasketController> _logger;
    private readonly IBasketRepository _basketRepository;
    private readonly IIdentityService _identityService;
    private readonly IEventBus _eventBus;

    public BasketController(ILogger<BasketController> logger, IBasketRepository basketRepository, IIdentityService identityService, IEventBus eventBus)
    {
        _logger = logger ?? throw new ArgumentNullException();
        _basketRepository = basketRepository ?? throw new ArgumentNullException();
        _identityService = identityService ?? throw new ArgumentNullException();
        _eventBus = eventBus ?? throw new ArgumentNullException();
    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(CustomerBasket), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetBasketByIdAsync([FromRoute] string id)
    {
        var basket = await _basketRepository.GetBasketAsync(id);

        return Ok(basket ?? new CustomerBasket(id));
    }

    [HttpPost]
    //[AllowAnonymous]
    [ProducesResponseType(typeof(CustomerBasket), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> UpdateBasketAsync(CustomerBasket basket)
    {
        return Ok(await _basketRepository.UpdateBasketAsync(basket));
    }

    [HttpPost]
    [Route("checkout")]
    [ProducesResponseType((int)HttpStatusCode.Accepted)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> CheckoutAsync([FromBody] BasketCheckout basketCheckout, [FromHeader(Name = "x-requestid")] string requestId)
    {
        var userId = _identityService.GetUserId();

        basketCheckout.RequestId = (Guid.TryParse(requestId, out Guid guid) && guid != Guid.Empty) ? guid : basketCheckout.RequestId;

        var basket = await _basketRepository.GetBasketAsync(userId);

        if (basket is null) return BadRequest();

        var userName = HttpContext.User.FindFirst(x => x.Type == ClaimTypes.Name)?.Value;

        var eventMessage = new UserCheckoutAcceptedIntegrationEvent(userId, userName, basketCheckout.City, basketCheckout.Street, basketCheckout.State, basketCheckout.Country,
            basketCheckout.CardNumber, basketCheckout.CardHolderName, basketCheckout.CardExpiration, basketCheckout.CardSecurityNumber, basketCheckout.CardTypeId,
            basketCheckout.Buyer, basketCheckout.RequestId, basket);

        try
        {
            _eventBus.Publish(eventMessage);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "ERROR Publishing integration event: {IntegrationEventId} from {AppName}", eventMessage.Id, Program.AppName);
            throw;
        }

        return Accepted();
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(typeof(void), (int)HttpStatusCode.OK)]
    public async Task DeleteBasketAsync(string id)
    {
        await _basketRepository.DeleteBasketAsync(id);
    }
}
