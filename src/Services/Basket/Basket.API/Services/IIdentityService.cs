﻿namespace Services.Basket.API.Services;
public interface IIdentityService
{
    string? GetUserId();
}
