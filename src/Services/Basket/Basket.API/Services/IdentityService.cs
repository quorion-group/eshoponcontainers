﻿namespace Services.Basket.API.Services;
public class IdentityService : IIdentityService
{
    private readonly IHttpContextAccessor _contextAccessor;

    public IdentityService(IHttpContextAccessor contextAccessor)
    {
        _contextAccessor = contextAccessor ?? throw new ArgumentNullException(nameof(contextAccessor));
    }

    public string? GetUserId()
    {
        return _contextAccessor.HttpContext?.User.FindFirst("sub")?.Value;
    }
}
