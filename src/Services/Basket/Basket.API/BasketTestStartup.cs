﻿namespace Services.Basket.API;
public class BasketTestStartup : Startup
{
    public BasketTestStartup(IConfiguration configuration) : base(configuration)
    {
    }

    protected override void ConfigureAuth(IApplicationBuilder app)
    {
        if (Configuration["isTest"] == bool.TrueString.ToLowerInvariant())
        {
            app.UseMiddleware<AutoAuthorizationMiddleware>();
            app.UseAuthorization();
        }
        else base.ConfigureAuth(app);
    }
}
