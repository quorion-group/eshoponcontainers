using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Services.Basket.API.Model;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Services.Basket.FunctionalTests;
public class BasketControllerTests
{
    [Fact]
    public async Task CreateOrUpdateBasket()
    {
        using var testServer = new BasketTestBase().CreateTestServer();
        using var basketClient = testServer.CreateClient();
        string content = JsonConvert.SerializeObject(new CustomerBasket
        {
            BuyerId = "a",
            Items = new List<BasketItem>
                {
                    new()
                    {
                        Id = Guid.NewGuid().ToString(),
                        UnitPrice = 80,
                        OldUnitPrice = 80,
                        PictureUrl = "url",
                        ProductId = 2,
                        ProductName = "Vinhomes Time City",
                        Quantity = 3
                    }
                }
        });

        var res = await basketClient.PostAsync("api/v1/basket", new StringContent(content, Encoding.UTF8, "application/json"));

        Assert.Equal(System.Net.HttpStatusCode.OK, res.StatusCode);
    }
}
