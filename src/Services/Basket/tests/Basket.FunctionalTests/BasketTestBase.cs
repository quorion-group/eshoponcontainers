﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Services.Basket.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Services.Basket.FunctionalTests;
public class BasketTestBase
{
    public TestServer CreateTestServer()
    {
        var webHostBuilder = new WebHostBuilder()
            .UseContentRoot(Path.GetDirectoryName(Assembly.GetAssembly(typeof(BasketTestBase)).Location))
            .ConfigureAppConfiguration(builder =>
            {
                builder.AddJsonFile("appsettings.json", optional: false)
                .AddEnvironmentVariables();
            })
            .UseStartup<BasketTestStartup>();

        return new TestServer(webHostBuilder);
    }
}
