using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Services.Catalog.API;
using Services.Catalog.API.Controllers;
using Services.Catalog.API.Infrastructure;
using Services.Catalog.API.IntegrationEvents;
using Services.Catalog.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests.CatalogControllerTests;
public class GetItemsAsyncTests : CatalogControllerTestsBase
{
    [Fact]
    public async Task GetItemsByIds_ReturnListOfItemsInDb()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        await dbContext.AddRangeAsync(new List<CatalogItem>
            {
                new CatalogItem { Id = 1, Name = "item1", Description = "item 1", CatalogBrandId = 1, CatalogTypeId = 2, PictureFileName = "item1.png" },
                new CatalogItem { Id = 2, Name = "item2", Description = "item 2", CatalogBrandId = 2, CatalogTypeId = 1, PictureFileName = "item2.png" },
            });
        await dbContext.SaveChangesAsync();

        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemsAsync(0, 0, "1,2");

        Assert.IsType<OkObjectResult>(response);
        var result = response as OkObjectResult;
        Assert.NotNull(result);
        var items = result.Value as List<CatalogItem>;
        Assert.NotNull(items);
        Assert.Equal(2, items.Count());
        var item1 = items[0];
        Assert.Equal(1, item1.Id);
        Assert.Equal("item1", item1.Name);
        Assert.Equal("item 1", item1.Description);
        Assert.Equal(1, item1.CatalogBrandId);
        Assert.Equal(2, item1.CatalogTypeId);
        Assert.Equal(catalogSetting.Value.PicBaseUrl.Replace("[0]", "1"), item1.PictureUri);
    }

    [Fact]
    public async Task GetItemsByPaging_Return1ItemOn1stPage()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        await dbContext.AddRangeAsync(new List<CatalogItem>
            {
                new CatalogItem { Id = 1, Name = "item1", Description = "item 1", CatalogBrandId = 1, CatalogTypeId = 2, PictureFileName = "item1.png" },
                new CatalogItem { Id = 2, Name = "item2", Description = "item 2", CatalogBrandId = 2, CatalogTypeId = 1, PictureFileName = "item2.png" },
                new CatalogItem { Id = 3, Name = "item3", Description = "item 3", CatalogBrandId = 2, CatalogTypeId = 1, PictureFileName = "item3.png" },
            });
        await dbContext.SaveChangesAsync();

        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemsAsync(1, 1);

        Assert.IsType<OkObjectResult>(response);
        var result = response as OkObjectResult;
        Assert.NotNull(result);
        var model = result.Value as PaginateItemsViewModel<CatalogItem>;
        Assert.NotNull(model);
        Assert.Single(model.Items);
        Assert.Equal(3, model.TotalRecord);
        var item = model.Items.First();
        Assert.NotNull(item);
        Assert.Equal("item1", item.Name);
        Assert.Equal("item 1", item.Description);
        Assert.Equal(1, item.CatalogBrandId);
        Assert.Equal(2, item.CatalogTypeId);
        Assert.Equal(catalogSetting.Value.PicBaseUrl.Replace("[0]", "1"), item.PictureUri);
    }

    [Fact]
    public async Task GetItemsByPaging_Return1ItemOn2ndPage()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        await dbContext.AddRangeAsync(new List<CatalogItem>
            {
                new CatalogItem { Id = 1, Name = "item1", Description = "item 1", CatalogBrandId = 1, CatalogTypeId = 2, PictureFileName = "item1.png" },
                new CatalogItem { Id = 2, Name = "item2", Description = "item 2", CatalogBrandId = 2, CatalogTypeId = 1, PictureFileName = "item2.png" },
                new CatalogItem { Id = 3, Name = "item3", Description = "item 3", CatalogBrandId = 2, CatalogTypeId = 1, PictureFileName = "item3.png" },
            });
        await dbContext.SaveChangesAsync();

        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemsAsync(2, 2);

        Assert.IsType<OkObjectResult>(response);
        var result = response as OkObjectResult;
        Assert.NotNull(result);
        var model = result.Value as PaginateItemsViewModel<CatalogItem>;
        Assert.NotNull(model);
        Assert.Single(model.Items);
        Assert.Equal(3, model.TotalRecord);
        var item = model.Items.First();
        Assert.NotNull(item);
        Assert.Equal("item3", item.Name);
        Assert.Equal("item 3", item.Description);
        Assert.Equal(2, item.CatalogBrandId);
        Assert.Equal(1, item.CatalogTypeId);
        Assert.Equal(catalogSetting.Value.PicBaseUrl.Replace("[0]", "3"), item.PictureUri);
    }

    [Fact]
    public async Task ReturnBadRequest_WhenIdInvalid()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemsAsync(0, 0, "a");

        Assert.IsType<BadRequestObjectResult>(response);
    }
}
