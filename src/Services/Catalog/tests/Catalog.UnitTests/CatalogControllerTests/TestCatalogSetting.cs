﻿using Microsoft.Extensions.Options;
using Services.Catalog.API;

namespace Catalog.UnitTests.CatalogControllerTests;

public class TestCatalogSetting : IOptionsSnapshot<CatalogSetting>
{
    public CatalogSetting Value => new CatalogSetting
    {
        PicBaseUrl = "image.com/[0]/pic"
    };

    public CatalogSetting Get(string name) => Value;
}
