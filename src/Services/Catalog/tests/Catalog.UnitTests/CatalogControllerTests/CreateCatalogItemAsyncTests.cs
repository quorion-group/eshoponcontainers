﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using Services.Catalog.API;
using Services.Catalog.API.Controllers;
using Services.Catalog.API.Infrastructure;
using Services.Catalog.API.IntegrationEvents;
using Services.Catalog.API.Model;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests.CatalogControllerTests;

public class DeleteCatalogItemAsyncTests : CatalogControllerTestsBase
{
    [Fact]
    public async Task ReturnNotFound_IfItemNotExist()
    {
        CatalogContext dbContext = GenerateCatalogContext(); IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();

        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.DeleteCatalogItemAsync(1);

        Assert.IsType<NotFoundResult>(response);
    }

    [Fact]
    public async Task ReturnOk_IfItemExisted()
    {
        CatalogContext dbContext = GenerateCatalogContext(); IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        dbContext.Add(new CatalogItem { Id = 1, Name = "a", Description = "a" });
        await dbContext.SaveChangesAsync();

        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.DeleteCatalogItemAsync(1);

        Assert.IsType<OkResult>(response);
        Assert.Empty(dbContext.CatalogItems);
    }
}
public class CreateCatalogItemAsyncTests : CatalogControllerTestsBase
{
    [Fact]
    public async Task InsertNewRecordToDB()
    {
        CatalogContext dbContext = GenerateCatalogContext(); IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();

        dbContext.Add(new CatalogBrand { Brand = "a", Id = 1 });
        dbContext.Add(new CatalogType { Type = "b", Id = 1 });
        await dbContext.SaveChangesAsync();

        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var creadModel = new CatalogItemCreateModel
        {
            CatalogBrandId = 1,
            CatalogTypeId = 1,
            Name = "a",
            Description = "a",
            PictureFileName = "a",
            Price = 1
        };

        var response = await catalogController.CreateCatalogItemAsync(creadModel);

        Assert.IsType<OkObjectResult>(response);
        Assert.Single(dbContext.CatalogItems.Where(ci => ci.Id > 0));
    }
}
