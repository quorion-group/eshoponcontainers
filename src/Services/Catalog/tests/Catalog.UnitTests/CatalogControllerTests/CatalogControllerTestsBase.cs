﻿using Microsoft.EntityFrameworkCore;
using Services.Catalog.API.Infrastructure;
using System;

namespace Catalog.UnitTests.CatalogControllerTests
{
    public class CatalogControllerTestsBase
    {
        protected CatalogContext GenerateCatalogContext()
        {
            var dbOptions = new DbContextOptionsBuilder<CatalogContext>()
                            .UseInMemoryDatabase(Guid.NewGuid().ToString())
                            .Options;
            var dbContext = new CatalogContext(dbOptions);
            return dbContext;
        }
    }
}