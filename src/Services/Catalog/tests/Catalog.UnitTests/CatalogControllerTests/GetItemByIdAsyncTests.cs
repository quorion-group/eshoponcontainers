﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Moq;
using Services.Catalog.API;
using Services.Catalog.API.Controllers;
using Services.Catalog.API.Infrastructure;
using Services.Catalog.API.IntegrationEvents;
using Services.Catalog.API.Model;
using System.Threading.Tasks;
using Xunit;

namespace Catalog.UnitTests.CatalogControllerTests;
public class GetItemByIdAsyncTests : CatalogControllerTestsBase
{
    [Theory]
    [InlineData(-1)]
    [InlineData(0)]
    public async Task ReturnBadRequest_WhenIdLessThanOrEqualToZero(int id)
    {
        CatalogContext dbContext = GenerateCatalogContext();
        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemByIdAsync(id);

        Assert.IsType<BadRequestResult>(response);
    }

    [Fact]
    public async Task ReturnNotFound_IfItemNotExistInDb()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);

        var response = await catalogController.GetItemByIdAsync(1);

        Assert.IsType<NotFoundResult>(response);
    }

    [Fact]
    public async Task ReturnItem_IfItExistInDb()
    {
        CatalogContext dbContext = GenerateCatalogContext();
        IOptionsSnapshot<CatalogSetting> catalogSetting = new TestCatalogSetting();
        dbContext.Add(new CatalogItem
        {
            Id = 1,
            Name = "a",
            Description = "b",
            CatalogBrandId = 1,
            CatalogTypeId = 2,
            PictureFileName = "a.png"
        });
        await dbContext.SaveChangesAsync();

        ICatalogIntegrationEventService catalogIntegrationEventService = new Mock<ICatalogIntegrationEventService>().Object;
        var catalogController = new CatalogController(dbContext, catalogSetting, catalogIntegrationEventService);
        var response = await catalogController.GetItemByIdAsync(1);

        Assert.IsType<OkObjectResult>(response);
        var result = response as OkObjectResult;
        Assert.NotNull(result);
        var item = result.Value as CatalogItem;
        Assert.NotNull(item);
        Assert.NotNull(item);
        Assert.Equal("a", item.Name);
        Assert.Equal("b", item.Description);
        Assert.Equal(1, item.CatalogBrandId);
        Assert.Equal(2, item.CatalogTypeId);
        Assert.Equal(catalogSetting.Value.PicBaseUrl.Replace("[0]", "1"), item.PictureUri);
    }
}
