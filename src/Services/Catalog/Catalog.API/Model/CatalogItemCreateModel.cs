﻿using Services.Catalog.API.Infrastructure;
using System.ComponentModel.DataAnnotations;

namespace Services.Catalog.API.Model;

public class CatalogItemCreateModel : IValidatableObject
{
    [Required]
    [MinLength(16)]
    [MaxLength(50)]
    public string Name { get; set; }

    [MaxLength(512)]
    public string Description { get; set; }

    public decimal Price { get; set; }

    [MaxLength(256)]
    public string PictureFileName { get; set; }

    public int CatalogBrandId { get; set; }

    public int CatalogTypeId { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        var result = new List<ValidationResult>();

        var context = validationContext.GetService<CatalogContext>();
        if (context == null) throw new ArgumentNullException();

        if (CatalogBrandId < 0)
            result.Add(new ValidationResult("CatalogBrandId must be greater than or equal to 0", new List<string> { nameof(CatalogBrandId) }));
        else if (CatalogBrandId > 0)
        {
            var brand = context.CatalogBrands.FirstOrDefault(brand => brand.Id == CatalogBrandId);
            if (brand is null)
                result.Add(new ValidationResult("Brand not exist with the given id", new List<string> { nameof(CatalogBrandId) }));
        }

        if (CatalogTypeId < 0)
            result.Add(new ValidationResult("CatalogTypeId must be greater than or equal to 0", new List<string> { nameof(CatalogTypeId) }));
        else if (CatalogTypeId > 0)
        {
            var type = context.CatalogTypes.FirstOrDefault(type => type.Id == CatalogTypeId);
            if (type is null)
                result.Add(new ValidationResult("Type not exist with the given id", new List<string> { nameof(CatalogTypeId) }));
        }

        return result;
    }
}
