﻿namespace Services.Catalog.API.Model;

public class CatalogItemUpdateModel : CatalogItemCreateModel
{
    public int Id { get; set; }

    public int AvailableStock { get; set; }

    public int RestockThreshold { get; set; }

    public int MaxstockThreshold { get; set; }

    public bool OnReorder { get; set; }
}
