﻿namespace Services.Catalog.API.Model;

public class PaginateItemsViewModel<TEntity> where TEntity: class
{
    public int PageIndex { get; private set; }

    public int PageSize { get; private set; }

    public long TotalRecord { get; private set; }

    public IEnumerable<TEntity> Items { get; private set; }

    public PaginateItemsViewModel(int pageIndex, int pageSize, long totalRecord, IEnumerable<TEntity> items)
    {
        PageIndex = pageIndex;
        PageSize = pageSize;
        TotalRecord = totalRecord;
        Items = items;
    }
}
