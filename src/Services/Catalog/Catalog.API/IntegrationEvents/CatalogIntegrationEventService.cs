﻿using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.Events;
using BuildingBlocks.IntegrationEventLogEF;
using BuildingBlocks.IntegrationEventLogEF.Services;
using BuildingBlocks.IntegrationEventLogEF.Utilities;
using Microsoft.EntityFrameworkCore;
using Services.Catalog.API.Infrastructure;
using System.Reflection;

namespace Services.Catalog.API.IntegrationEvents;

public class CatalogIntegrationEventService : ICatalogIntegrationEventService, IDisposable
{
    private readonly CatalogContext _catalogContext;
    private readonly IEventBus _eventBus;
    private readonly IIntegrationEventLogService _eventLogService;
    private readonly ILogger<CatalogIntegrationEventService> _logger;
    private volatile bool _disposedValue;

    public CatalogIntegrationEventService(CatalogContext catalogContext, IEventBus eventBus, Func<DbContextOptions<IntegrationEventLogContext>, IIntegrationEventLogService> integrationEventLogServiceFactory, ILogger<CatalogIntegrationEventService> logger)
    {
        _catalogContext = catalogContext;
        _eventBus = eventBus;
        _logger = logger;
        _eventLogService = integrationEventLogServiceFactory(
            new DbContextOptionsBuilder<IntegrationEventLogContext>()
            .UseSqlServer(catalogContext.Database.GetDbConnection())
            .Options);
    }

    public async Task SaveEventAndCatalogChangeContextAsync(IntegrationEvent evt)
    {
        _logger.LogInformation("---- CatalogIntegrationEventService - Saving changes and integrationEvent: {eventId}", evt.Id);

        await ResilientTransaction.New(_catalogContext).ExecuteAsync(async () =>
        {
            await _catalogContext.SaveChangesAsync();

            await _eventLogService.SaveEventAsync(evt, _catalogContext.Database.CurrentTransaction);
        });
    }
    
    public async Task PublishThroughEventBusAsync(IntegrationEvent evt)
    {
        try
        {
            _logger.LogInformation("---- Publishing integration event: {eventId} from {appName} - ({@IntegrationEvent})", evt.Id, Program.AppName, evt);

            await _eventLogService.MarkEventAsInProgressAsync(evt.Id);

            _eventBus.Publish(evt);

            await _eventLogService.MarkEventAsPublishedAsync(evt.Id);
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "ERROR Publishing integration event: {eventId} from {appName} - ({@IntegrationEvent})", evt.Id, Program.AppName, evt);

            await _eventLogService.MarkEventAsPublishedFailedAsync(evt.Id);
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                (_eventLogService as IDisposable)?.Dispose();
            }

            _disposedValue = true;
        }
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }
}
