﻿using CatalogApi;
using Grpc.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Services.Catalog.API.Extensions;
using Services.Catalog.API.Infrastructure;
using static CatalogApi.Catalog;

namespace Services.Catalog.API.Grpc;
public class CatalogService : CatalogBase
{
    private readonly IOptionsSnapshot<CatalogSetting> _setting;
    private readonly CatalogContext _dbContext;
    private readonly ILogger _logger;

    public CatalogService(ILogger logger, CatalogContext dbContext, IOptionsSnapshot<CatalogSetting> setting)
    {
        _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _setting = setting ?? throw new ArgumentNullException(nameof(setting));
    }

    public override async Task<CatalogItemResponse>? GetItemById(CatalogItemRequest request, ServerCallContext context)
    {
        _logger.LogInformation("Begin grpc call CatalogService.GeetItemById for product id {Id}", request.Id);

        if (request.Id <= 0)
        {
            context.Status = new Status(StatusCode.FailedPrecondition, $"Id must be greater than 0 (received {request.Id})");
            return null;
        }

        var item = await _dbContext.CatalogItems
            .Include(ci => ci.CatalogBrand)
            .Include(ci => ci.CatalogType)
            .FirstOrDefaultAsync(ci => ci.Id == request.Id);

        if (item is null)
        {
            context.Status = new Status(StatusCode.NotFound, $"Product with id {request.Id} do not exist");
            return null;
        }

        return new CatalogItemResponse
        {
            Id = request.Id,
            Name = item.Name,
            Description = item.Description,
            Price = (double)item.Price,
            PictureFileName = item.PictureFileName,
            PictureUri = item.PictureUri,
            CatalogBrand = item.CatalogBrand is null ? null : new CatalogBrand
            {
                Brand = item.CatalogBrand.Brand,
                Id = item.CatalogBrand.Id
            },
            CatalogType = item.CatalogType is null ? null : new CatalogType
            {
                Type = item.CatalogType.Type,
                Id = item.CatalogType.Id,
            },
            AvailableStock = item.AvailableStock,
            RestockThreshold = item.RestockThreshold,
            MaxStockThreshold = item.MaxstockThreshold,
            OnReorder = item.OnReorder
        };
    }

    public override async Task<PaginateItemsResponse>? GetItemsByIds(CatalogItemsRequest request, ServerCallContext context)
    {
        if (!string.IsNullOrWhiteSpace(request.Ids))
        {
            var items = await GetCatalogItemByIdsAsync(request.Ids);
            if (!items.Any())
            {
                context.Status = new Status(StatusCode.FailedPrecondition, "ids is invalid. Must be comma-separated list of numbers");
                return null;
            }

            return GeneratePaginateItemsReponse(items, items.Count(), request.PageIndex, request.PageSize);
        }

        var totalItems = await _dbContext.CatalogItems.LongCountAsync();
        var itemsOnPage = await _dbContext.CatalogItems
            .OrderBy(c => c.Name)
            .Skip((request.PageIndex - 1) * request.PageSize)
            .Take(request.PageSize)
            .ToListAsync();

        PopulateProductUrlToItems(itemsOnPage);

        return GeneratePaginateItemsReponse(itemsOnPage, totalItems, request.PageIndex, request.PageSize);
    }

    private static PaginateItemsResponse GeneratePaginateItemsReponse(IEnumerable<Model.CatalogItem> items, long count, int pageIndex, int pageSize)
    {
        var result = new PaginateItemsResponse
        {
            Count = count,
            PageIndex = pageIndex,
            PageSize = pageSize
        };

        foreach (var item in items)
        {
            result.Data.Add(new CatalogItemResponse
            {
                Id = item.Id,
                Name = item.Name,
                Description = item.Description,
                CatalogBrand = item.CatalogBrand is null ? null : new CatalogBrand
                {
                    Brand = item.CatalogBrand.Brand,
                    Id = item.CatalogBrandId
                },
                CatalogType = item.CatalogType is null ? null : new CatalogType
                {
                    Type = item.CatalogType.Type,
                    Id = item.CatalogTypeId
                },
                AvailableStock = item.AvailableStock,
                RestockThreshold = item.RestockThreshold,
                MaxStockThreshold = item.MaxstockThreshold,
                OnReorder = item.OnReorder,
            });
        }

        return result;
    }

    private async Task<IEnumerable<Model.CatalogItem>> GetCatalogItemByIdsAsync(string ids)
    {
        var numIds = ids.Split(",").Select(id => (OK: int.TryParse(id, out int res), Value: res));
        if (numIds.ToList().Exists(id => !id.OK))
            return new List<Model.CatalogItem>();

        var idsCondition = numIds.Select(id => id.Value);
        var catalogItems = await _dbContext.CatalogItems
            .Where(c => idsCondition.Contains(c.Id))
            .ToListAsync();

        PopulateProductUrlToItems(catalogItems);

        return catalogItems;
    }

    private void PopulateProductUrlToItems(List<Model.CatalogItem> catalogItems)
    {
        catalogItems.ForEach(item => item.FillProductUrl(_setting.Value.PicBaseUrl));
    }
}
