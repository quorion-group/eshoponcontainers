﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using Services.Catalog.API.Model;
using System.IO.Compression;

namespace Services.Catalog.API.Infrastructure
{
    public class CatalogContextSeed
    {
        public async Task SeedAsync(CatalogContext context, IWebHostEnvironment env, ILogger<CatalogContextSeed> logger)
        {
            AsyncRetryPolicy policy = CreateRetryPolicy(logger, nameof(CatalogContextSeed));

            await policy.ExecuteAsync(async () =>
            {
                if (!context.CatalogBrands.Any())
                {
                    await context.AddRangeAsync(GenerateCatalogBrands());
                    await context.SaveChangesAsync();
                }

                if (!context.CatalogTypes.Any())
                {
                    await context.AddRangeAsync(GenerateCatalogTypes());
                    await context.SaveChangesAsync();
                }

                if (!context.CatalogItems.Any())
                {
                    await context.AddRangeAsync(GenerateCatalogItems());
                    await context.SaveChangesAsync();

                    GetCatalogItemPictures(env.ContentRootPath, env.WebRootPath);
                }
            });
        }

        private void GetCatalogItemPictures(string contentRootPath, string picturePath)
        {
            if (picturePath != null)
            {
                DirectoryInfo directory = new(picturePath);

                if (!Directory.Exists(picturePath))
                    directory.Create();

                foreach (FileInfo file in directory.GetFiles())
                {
                    file.Delete();
                }

                string zipFileCatalogItemPictures = Path.Combine(contentRootPath, "Setup", "CatalogItems.zip");
                ZipFile.ExtractToDirectory(zipFileCatalogItemPictures, picturePath);
            }
        }

        private static List<CatalogItem> GenerateCatalogItems()
        {
            return new List<CatalogItem>
                    {
                        new CatalogItem { Id = 1, CatalogBrandId = 1, CatalogTypeId = 1, AvailableStock = 100, Name = "Vinmec Time City", Description = "Vinmec Time City", Price = 50, PictureFileName = "1.png" },
                        new CatalogItem { Id = 2, CatalogBrandId = 1, CatalogTypeId = 4, AvailableStock = 100, Name = "Vinhomes Time City", Description = "Vinhomes Time City", Price = 100, PictureFileName = "2.png" },
                        new CatalogItem { Id = 3, CatalogBrandId = 1, CatalogTypeId = 4, AvailableStock = 100, Name = "Vinhomes Ocean Park", Description = "Vinhomes Ocean Park", Price = 70, PictureFileName = "3.png" },
                        new CatalogItem { Id = 4, CatalogBrandId = 3, CatalogTypeId = 3, AvailableStock = 100, Name = "Vinbus Limited Company", Description = "Vinbus Limited Company", Price = 60, PictureFileName = "4.png" },
                        new CatalogItem { Id = 5, CatalogBrandId = 2, CatalogTypeId = 2, AvailableStock = 100, Name = "Vinschool Riverside", Description = "Vinschool Riverside", Price = 50, PictureFileName = "5.png" },
                        new CatalogItem { Id = 6, CatalogBrandId = 3, CatalogTypeId = 3, AvailableStock = 100, Name = "Vinfast Royal City", Description = "Vinfast Royal City", Price = 60, PictureFileName = "6.png" },
                    };
        }

        private static List<CatalogType> GenerateCatalogTypes()
        {
            return new List<CatalogType>
                    {
                        new CatalogType { Id = 1, Type = "Health Care"},
                        new CatalogType { Id = 2, Type = "Education"},
                        new CatalogType { Id = 3, Type = "Electric Vehicle"},
                        new CatalogType { Id = 4, Type = "Real state"},
                    };
        }

        private static List<CatalogBrand> GenerateCatalogBrands()
        {
            return new List<CatalogBrand>
                    {
                        new CatalogBrand{ Id = 1, Brand = "Vinhomes"},
                        new CatalogBrand{ Id = 2, Brand = "Vinmec"},
                        new CatalogBrand{ Id = 3, Brand = "Vinfast"},
                        new CatalogBrand{ Id = 4, Brand = "Vinmart"},
                        new CatalogBrand{ Id = 5, Brand = "VinUni"},
                    };
        }

        private static AsyncRetryPolicy CreateRetryPolicy(ILogger<CatalogContextSeed> logger, string prefix, int retries = 3)
        {
            return Policy.Handle<SqlException>()
                            .WaitAndRetryAsync(
                                retryCount: retries,
                                sleepDurationProvider: retry => TimeSpan.FromSeconds(5),
                                onRetry: (exception, timespan, retryCount, context) =>
                                {
                                    logger.LogWarning(exception, "[{prefix}] Exception {ExceptionType} with message {Message} detected on attempt {retry} of {retries}", prefix, exception.GetType().Name, exception.Message, retryCount, retries);
                                }
                            );
        }
    }
}
