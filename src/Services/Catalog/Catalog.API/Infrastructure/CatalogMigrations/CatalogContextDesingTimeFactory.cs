﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Services.Catalog.API.Infrastructure.CatalogMigrations;

public class CatalogContextDesingTimeFactory : IDesignTimeDbContextFactory<CatalogContext>
{
    public CatalogContext CreateDbContext(string[] args)
    {
        var optionBuilder = new DbContextOptionsBuilder<CatalogContext>();
        optionBuilder.UseSqlServer(".");

        return new CatalogContext(optionBuilder.Options);
    }
}
