﻿using BuildingBlocks.IntegrationEventLogEF;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Services.Catalog.API.Infrastructure.IntegrationEventMigrations;

public class IntegrationEventLogContextDesignTimeFactory : IDesignTimeDbContextFactory<IntegrationEventLogContext>
{
    public IntegrationEventLogContext CreateDbContext(string[] args)
    {
        var configuration = Program.GetConfiguration();
        var optionBuilder = new DbContextOptionsBuilder<IntegrationEventLogContext>();
        optionBuilder.UseSqlServer(configuration["ConnectionString"], sqloptions =>
        {
            sqloptions.MigrationsAssembly(GetType().Assembly.GetName().Name);
        });

        return new IntegrationEventLogContext(optionBuilder.Options);
    }
}
