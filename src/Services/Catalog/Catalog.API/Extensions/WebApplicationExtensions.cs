﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Polly;

namespace Services.Catalog.API.Extensions;
public static class WebApplicationExtensions
{
    public static IWebHost MigrateDbContext<TContext>(this IWebHost app, Action<TContext, IServiceProvider> seeder) where TContext : DbContext
    {
        try
        {
            Serilog.Log.Information("Migrating database associated with context {DbContextName}", typeof(TContext).Name);

            var retry = Policy.Handle<SqlException>()
                              .WaitAndRetry(new TimeSpan[]
                              {
                                    TimeSpan.FromSeconds(3),
                                    TimeSpan.FromSeconds(5),
                                    TimeSpan.FromSeconds(8),
                              });
            retry.Execute(() => InvokeSeeder(app, seeder));

            Serilog.Log.Information("Migrated database associated with context {DbContextName}", typeof(TContext).Name);
        }
        catch (Exception ex)
        {
            Serilog.Log.Error(ex, "An error occurred while migrating the database used on context {DbContextName}", typeof(TContext).Name);
        }

        return app;
    }

    private static void InvokeSeeder<TContext>(IWebHost app, Action<TContext, IServiceProvider> seeder) where TContext : DbContext
    {
        using var scope = app.Services.CreateScope();
        var dbContext = scope.ServiceProvider.GetService<TContext>();
        if (dbContext != null)
        {
            dbContext.Database.Migrate();
            seeder?.Invoke(dbContext, scope.ServiceProvider);
        }
    }
}

