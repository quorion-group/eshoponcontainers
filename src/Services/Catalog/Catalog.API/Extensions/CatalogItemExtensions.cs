﻿using Services.Catalog.API.Model;

namespace Services.Catalog.API.Extensions;

public static class CatalogItemExtensions
{
    public static void FillProductUrl(this CatalogItem item, string picBaseUrl)
    {
        if (item != null && !string.IsNullOrWhiteSpace(picBaseUrl))
            item.PictureUri = picBaseUrl.Replace("[0]", item.Id.ToString());
    }
}

