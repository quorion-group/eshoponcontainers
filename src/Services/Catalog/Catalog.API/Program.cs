using BuildingBlocks.IntegrationEventLogEF;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Serilog;
using Services.Catalog.API;
using Services.Catalog.API.Extensions;
using Services.Catalog.API.Infrastructure;
using System.Net;

var configuration = GetConfiguration();

Log.Logger = new LoggerConfiguration()
    .ReadFrom.Configuration(configuration)
    .CreateLogger();

try
{
    var webHost = BuildWebHost(args, configuration);

    webHost.MigrateDbContext<CatalogContext>((contex, serviceProvider) =>
    {
        var environment = serviceProvider.GetService<IWebHostEnvironment>();
        var logger = serviceProvider.GetService<ILogger<CatalogContextSeed>>();
        if (environment != null && logger != null)
            new CatalogContextSeed().SeedAsync(contex, environment, logger).Wait();
    }).MigrateDbContext<IntegrationEventLogContext>((_, _) => { });

    webHost.Run();

    Log.Information("Application is started");

}
catch (Exception ex)
{
    Log.Error("Fail to start application. {Message} - {StackTrace}", ex.Message, ex.StackTrace);
}
finally
{
    Log.CloseAndFlush();
}

IWebHost BuildWebHost(string[] args, IConfiguration configuration) =>
    WebHost.CreateDefaultBuilder(args)
        .UseSerilog()
        .UseStartup<Startup>()
        .ConfigureAppConfiguration(x => x.AddConfiguration(configuration))
        .CaptureStartupErrors(false)
        .ConfigureKestrel(options =>
        {
            var port = GetDefinedPorts(configuration);
            options.Listen(IPAddress.Any, port.httpPort, listenOptions =>
            {
                listenOptions.Protocols = HttpProtocols.Http1AndHttp2;
            });
            options.Listen(IPAddress.Any, port.grpcPort, listenOptions =>
            {
                listenOptions.Protocols = HttpProtocols.Http2;
            });
        })
        .UseContentRoot(Directory.GetCurrentDirectory())
        .Build();

(int httpPort, int grpcPort) GetDefinedPorts(IConfiguration configuration)
{
    var grpcPort = configuration.GetValue("GRPC_PORT", 81);
    var httpPort = configuration.GetValue("HTTP_PORT", 80);

    return (httpPort, grpcPort);
}

public partial class Program
{
    public static string Namespace = typeof(CatalogSetting).Namespace;
    public static string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

    public static IConfiguration GetConfiguration()
    {
        return new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true)
            .AddEnvironmentVariables()
            .Build();
    }
}