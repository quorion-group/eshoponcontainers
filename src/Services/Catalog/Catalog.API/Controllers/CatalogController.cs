﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Services.Catalog.API.Extensions;
using Services.Catalog.API.Infrastructure;
using Services.Catalog.API.IntegrationEvents;
using Services.Catalog.API.IntegrationEvents.Events;
using Services.Catalog.API.Model;
using System.Net;

namespace Services.Catalog.API.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class CatalogController : Controller
{
    private readonly CatalogContext _context;
    private readonly IOptionsSnapshot<CatalogSetting> _setting;
    private readonly ICatalogIntegrationEventService _catalogIntegrationEventService;
    public CatalogController(CatalogContext context, IOptionsSnapshot<CatalogSetting> setting, ICatalogIntegrationEventService catalogIntegrationEventService)
    {
        _context = context;
        _setting = setting;
        _catalogIntegrationEventService = catalogIntegrationEventService;
    }

    [HttpGet]
    [Route("items")]
    [ProducesResponseType(typeof(IEnumerable<CatalogItem>), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> GetItemsAsync([FromQuery] int pageIndex = 1, [FromQuery] int pageSize = 10, string? ids = null)
    {
        if (!string.IsNullOrWhiteSpace(ids))
        {
            var items = await GetCatalogItemByIdsAsync(ids);
            if (!items.Any())
                return BadRequest("ids is invalid. Must be comma-separated list of numbers");

            return Ok(items);
        }

        var totalItems = await _context.CatalogItems.LongCountAsync();
        var itemsOnPage = await _context.CatalogItems
            .OrderBy(c => c.Name)
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        PopulateProductUrlToItems(itemsOnPage);

        var model = new PaginateItemsViewModel<CatalogItem>(pageIndex, pageSize, totalItems, itemsOnPage);

        return Ok(model);
    }

    private async Task<IEnumerable<CatalogItem>> GetCatalogItemByIdsAsync(string ids)
    {
        var numIds = ids.Split(",").Select(id => (OK: int.TryParse(id, out int res), Value: res));
        if (numIds.ToList().Exists(id => !id.OK))
            return new List<CatalogItem>();

        var idsCondition = numIds.Select(id => id.Value);
        var catalogItems = await _context.CatalogItems
            .Where(c => idsCondition.Contains(c.Id))
            .ToListAsync();

        PopulateProductUrlToItems(catalogItems);

        return catalogItems;
    }

    [HttpGet]
    [Route("items/{id:int}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(CatalogItem), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetItemByIdAsync(int id)
    {
        if (id <= 0) return BadRequest();

        var item = await _context.CatalogItems.SingleOrDefaultAsync(ci => ci.Id == id);

        if (item is null) return NotFound();

        item.FillProductUrl(_setting.Value.PicBaseUrl);

        return Ok(item);
    }

    [HttpGet]
    [Route("items/withname/{name:minlength(1)}")]
    [ProducesResponseType(typeof(PaginateItemsViewModel<CatalogItem>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetItemsWithNameAsync(string name, [FromQuery] int pageIndex = 1, [FromQuery] int pageSize = 10)
    {
        var query = _context.CatalogItems.Where(ci => ci.Name.StartsWith(name));

        var totalRecord = await query.LongCountAsync();
        var items = await query
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        PopulateProductUrlToItems(items);

        return Ok(new PaginateItemsViewModel<CatalogItem>(pageIndex, pageSize, totalRecord, items));
    }

    [HttpGet]
    [Route("items/type/{catalogTypeId}/brand/{catalogBrandId:int?}")]
    [ProducesResponseType(typeof(PaginateItemsViewModel<CatalogItem>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetItemsByTypeIdAndBrandIdAsync(int catalogTypeId, int? catalogBrandId, [FromQuery] int pageIndex = 1, [FromQuery] int pageSize = 10)
    {
        var query = _context.CatalogItems.Where(ci => ci.CatalogTypeId == catalogTypeId);

        if (catalogBrandId.HasValue)
            query = query.Where(ci => ci.CatalogBrandId == catalogBrandId);

        var totalRecord = await query.LongCountAsync();
        var items = await query
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        PopulateProductUrlToItems(items);

        return Ok(new PaginateItemsViewModel<CatalogItem>(pageIndex, pageSize, totalRecord, items));
    }

    [HttpGet]
    [Route("items/type/all/brand/{catalogBrandId:int?}")]
    [ProducesResponseType(typeof(PaginateItemsViewModel<CatalogItem>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetItemByBrandId(int? catalogBrandId, [FromQuery] int pageIndex, [FromQuery] int pageSize)
    {
        var query = _context.CatalogItems.AsQueryable();

        if (catalogBrandId.HasValue)
            query = query.Where(ci => ci.CatalogBrandId == catalogBrandId);

        var totalRecord = await query.LongCountAsync();
        var items = await query
            .Skip((pageIndex - 1) * pageSize)
            .Take(pageSize)
            .ToListAsync();

        PopulateProductUrlToItems(items);

        return Ok(new PaginateItemsViewModel<CatalogItem>(pageIndex, pageSize, totalRecord, items));
    }

    [HttpGet]
    [Route("catalogTypes")]
    [ProducesResponseType(typeof(List<CatalogType>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetAllTypes()
    {
        return Ok(await _context.CatalogTypes.ToListAsync());
    }

    [HttpGet]
    [Route("catalogBrands")]
    [ProducesResponseType(typeof(List<CatalogBrand>), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetAllBrands()
    {
        return Ok(await _context.CatalogBrands.ToListAsync());
    }

    [HttpPut]
    [Route("items")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.Created)]
    public async Task<IActionResult> UpdateProductAsync([FromBody] CatalogItemUpdateModel productToUpdate)
    {
        var catalogItem = await _context.CatalogItems.SingleOrDefaultAsync(ci => ci.Id == productToUpdate.Id);

        if (catalogItem is null) return NotFound();

        var oldPrice = catalogItem.Price;
        var newPrice = productToUpdate.Price;
        var raiseProductPriceChangeEvent = oldPrice != newPrice;
        
        MapUpdateModel(productToUpdate, catalogItem);

        _context.Update(catalogItem);

        if (raiseProductPriceChangeEvent)
        {
            var priceChangedEvent = new ProductPriceChangedIntegrationEvent(productToUpdate.Id, oldPrice, newPrice);

            await _catalogIntegrationEventService.SaveEventAndCatalogChangeContextAsync(priceChangedEvent);

            await _catalogIntegrationEventService.PublishThroughEventBusAsync(priceChangedEvent);
        }
        else
            await _context.SaveChangesAsync();

        return Ok(productToUpdate);
    }

    private static void MapUpdateModel(CatalogItemUpdateModel productToUpdate, CatalogItem catalogItem)
    {
        catalogItem.Price = productToUpdate.Price;
        catalogItem.AvailableStock = productToUpdate.AvailableStock;
        catalogItem.RestockThreshold = productToUpdate.RestockThreshold;
        catalogItem.MaxstockThreshold = productToUpdate.MaxstockThreshold;
        catalogItem.OnReorder = productToUpdate.OnReorder;
    }

    [HttpPost]
    [Route("items")]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.Conflict)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> CreateCatalogItemAsync([FromBody] CatalogItemCreateModel createdProduct)
    {
        CatalogItem item = GenerateCatalogItem(createdProduct);

        _context.CatalogItems.Add(item);
        await _context.SaveChangesAsync();

        return Ok(new { item.Id });
    }

    [HttpDelete]
    [Route("{id}")]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    public async Task<IActionResult> DeleteCatalogItemAsync([FromRoute] int id)
    {
        var item = await _context.CatalogItems.FirstOrDefaultAsync(ci => ci.Id == id);

        if (item is null) return NotFound();

        _context.Remove(item);
        await _context.SaveChangesAsync();

        return Ok();
    }

    private static CatalogItem GenerateCatalogItem(CatalogItemCreateModel createdProduct)
    {
        return new CatalogItem
        {
            Name = createdProduct.Name,
            Description = createdProduct.Description,
            Price = createdProduct.Price,
            PictureFileName = createdProduct.PictureFileName,
            CatalogBrandId = createdProduct.CatalogBrandId,
            CatalogTypeId = createdProduct.CatalogTypeId,
        };
    }

    private void PopulateProductUrlToItems(List<CatalogItem> catalogItems)
    {
        catalogItems.ForEach(item => item.FillProductUrl(_setting.Value.PicBaseUrl));
    }
}
