﻿using Microsoft.AspNetCore.Mvc;
using Services.Catalog.API.Infrastructure;

namespace Services.Catalog.API.Controllers;

[Route("/")]
public class HomeController : Controller
{
    [HttpGet]
    public IActionResult Index()
    {
        return new RedirectResult("~/swagger");
    }
}
