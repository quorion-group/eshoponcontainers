﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Services.Catalog.API.Infrastructure;
using System.Net;

namespace Services.Catalog.API.Controllers;

[ApiController]
public class PicController : Controller
{
    private readonly CatalogContext _context;
    private readonly IWebHostEnvironment _env;
    public PicController(CatalogContext context, IWebHostEnvironment env)
    {
        _context = context;
        _env = env;
    }

    [HttpGet]
    [Route("api/v1/catalog/items/{catalogItemId:int}/pic")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult> GetImageAsync(int catalogItemId)
    {
        if (catalogItemId < 0) return BadRequest();

        var item = await _context.CatalogItems.SingleOrDefaultAsync(ci => ci.Id == catalogItemId);

        if (item is null) return NotFound();

        var filePath = Path.Combine(_env.WebRootPath, item.PictureFileName);

        if (!System.IO.File.Exists(filePath)) return NotFound();

        var buffer = System.IO.File.ReadAllBytes(filePath);

        return base.File(buffer, GetMimeTypeFromImageExtension(Path.GetExtension(filePath)));
    }

    private static string GetMimeTypeFromImageExtension(string extension)
    {
        return extension switch
        {
            ".png" => "image/png",
            ".gif" => "image/gif",
            ".jpg" => "image/jpg",
            ".jpeg" => "image/jpg",
            ".bmp" => "image/bmp",
            ".tiff" => "image/tiff",
            ".wmf" => "image/wmf",
            ".jp2" => "image/jp2",
            ".svg" => "image/svg+xml",
            _ => "application/octet-stream"
        };
    }
}
