﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using BuildingBlocks.EventBus.Abstractions;
using BuildingBlocks.EventBus.EventBusRabbitMQ;
using BuildingBlocks.EventBusRabbitMQ;
using BuildingBlocks.IntegrationEventLogEF;
using BuildingBlocks.IntegrationEventLogEF.Services;
using EventBus;
using Microsoft.EntityFrameworkCore;
using RabbitMQ.Client;
using Services.Catalog.API.Extensions;
using Services.Catalog.API.Grpc;
using Services.Catalog.API.Infrastructure;
using Services.Catalog.API.IntegrationEvents;
using System.Reflection;

namespace Services.Catalog.API;
public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; private set; }

    public IServiceProvider ConfigureServices(IServiceCollection services)
    {
        services.AddGrpc().Services
            .AddCustomMVC()
            .AddCustomSetting(Configuration)
            .AddCustomDbContext(Configuration)
            .AddIntegrationService(Configuration)
            .AddEventBus(Configuration)
            .AddSwaggerGen();

        var containerBuilder = new ContainerBuilder();
        containerBuilder.Populate(services);

        return new AutofacServiceProvider(containerBuilder.Build());
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        env.ContentRootPath = Directory.GetCurrentDirectory();
        env.WebRootPath = "Pics";

        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapDefaultControllerRoute();
            endpoints.MapControllers();
            endpoints.MapGet("/_proto/", async ctx =>
            {
                ctx.Response.ContentType = "text/plain";
                using var fs = new FileStream(Path.Combine(env.ContentRootPath, "Proto", "catalog.proto"), FileMode.Open, FileAccess.Read);
                using var sr = new StreamReader(fs);
                while (!sr.EndOfStream)
                {
                    var line = await sr.ReadLineAsync();
                    if (line != "/* >>" || line != "<< */")
                    {
                        await ctx.Response.WriteAsync(line);
                    }
                }
            });
            endpoints.MapGrpcService<CatalogService>();
        });
    }
}

public static class CustomExtensionMethods
{
    public static IServiceCollection AddIntegrationService(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<Func<DbContextOptions<IntegrationEventLogContext>, IIntegrationEventLogService>>(sp =>
        {
            return (DbContextOptions<IntegrationEventLogContext> options) => new IntegrationEventLogService(options);
        });

        services.AddTransient<ICatalogIntegrationEventService, CatalogIntegrationEventService>();
        services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
        {
            var factory = new ConnectionFactory
            {
                HostName = configuration["EventBusHost"],
                DispatchConsumersAsync = true
            };

            if (!String.IsNullOrWhiteSpace(configuration["EventBusUserName"]))
                factory.UserName = configuration["EventBusUserName"];

            if (!String.IsNullOrWhiteSpace(configuration["EventBusPassword"]))
                factory.Password = configuration["EventBusPassword"];

            var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();

            int retryCount = 5;
            if (!string.IsNullOrWhiteSpace(configuration["EventBusRetryCount"])
                && int.TryParse(configuration["EventBusRetryCount"], out int retryCountResult))
            {
                retryCount = retryCountResult;
            }

            return new DefaultRabbitMQPersistentConnection(factory, logger, retryCount);
        });

        return services;
    }

    public static IServiceCollection AddEventBus(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IEventBus, EventBusRabbitMQ>(sp =>
        {
            var subcriptionClientName = configuration["SubscriptionClientName"];
            var persistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            var subManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
            var iLifeTimeScope = sp.GetRequiredService<ILifetimeScope>();

            return new EventBusRabbitMQ(persistentConnection, logger, subManager, subcriptionClientName, iLifeTimeScope);
        });
        services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();

        return services;
    }

    public static IServiceCollection AddCustomMVC(this IServiceCollection services)
    {
        services.AddControllers();
        services.AddCors(options =>
            options.AddPolicy("CorsPolicy", opt =>
                opt.SetIsOriginAllowed(_ => true)
                .AllowAnyHeader()
                .AllowAnyMethod()
                .AllowCredentials()
        ));

        return services;
    }

    public static IServiceCollection AddCustomSetting(this IServiceCollection services, IConfiguration configuration)
    {
        services.Configure<CatalogSetting>(configuration);

        return services;
    }

    public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<CatalogContext>(options =>
        {
            options.UseSqlServer(configuration["ConnectionString"],
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly(typeof(Program).GetTypeInfo().Assembly.GetName().Name); //Optional
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                });
        });
        services.AddDbContext<IntegrationEventLogContext>(options =>
        {
            options.UseSqlServer(configuration["ConnectionString"],
                sqlServerOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly(typeof(Program).GetTypeInfo().Assembly.GetName().Name);
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
                });
        });

        return services;
    }
}
